const express = require("express");
const AuthController = require("../controllers/AuthController");
const HomeController = require("../controllers/HomeController");

function routes() {
  const router = express.Router();

  //home controller
  router.get('/', makeRouter(HomeController.index));
  router.get("/login", makeRouter(AuthController.login));
  router.get("/account", makeRouter(HomeController.account));

  router.post("/login", makeRouter(AuthController.loginUser()));

  return router;
}

function makeRouter(apiController){
  return function(req,res){
    apiController(req,res);
  }
}
module.exports = routes;
